import React from "react";
import ReactDOM from "react-dom";

import "./index.less";

const RE_FLOAT = /^[-+]?(0|[1-9][0-9]*)([.][0-9]*)?$/;

function useComplexState(p) {
	const initial = p ? p.get(): {};
	const [ state, setState ] = React.useState(initial);

	function useProperty (property: string) {
		const val = state[property] || ""
		const setVal = value => {
			const _new = { ...state, [property]: value };
			p && p.set(_new);
			setState(_new);
		}
		return [ val, setVal ]
	}

	return { state, useProperty }
}

function useFloatField(state, property: string) {
	const [ value, setValue ] = state.useProperty(property)
	const onChange = ({ target }) => {
		const isValid = RE_FLOAT.test(target.value);
		if (!isValid && value === undefined) return setValue("");
		if (!isValid) return setValue(value);
		setValue(target.value);
	}
	return { value, onChange, type: "text" }
}

function useIntegerField(state, property: string) {
	const [ value, setValue ] = state.useProperty(property)
	const onChange = ({ target }) => setValue(parseInt(target.value));
	return { value, onChange, type: "text" }
}


function useField(state, property: string, type: string = "text") {
	const [ value, setValue ] = state.useProperty(property)
	const onChange = ({ target }) => setValue(target.value);		
	return { value, onChange, type: type }
}

function useCheckbox(state, property: string) {
	const [ checked, setValue ] = state.useProperty(property)
	const onChange = () => setValue(!checked);		
	return { checked, onChange, type: "checkbox" }
}

function ParameterInput({schema, state}) {

	if (schema.enum) {
		const values = schema.enum, names = schema.enumNames || values;
		return <select {...useField(state, schema.$id, "select")}>
			{values.map((value, i) => <option key={value} value={value}>{names[i]}</option>)}
		</select>
	}

	switch (schema.type) {
		case "string" : return <input {...useField(state, schema.$id, "text")}/>
		case "number" : return <input {...useFloatField(state, schema.$id)}/>
		case "integer": return <input {...useIntegerField(state, schema.$id)}/>			
		case "boolean": return <input {...useCheckbox(state, schema.$id)}/>
		case "array"  : throw new Error("Not yet implemented");
		default: throw new Error("Incorrect schema.type value or missing");
	}
}

function resolveType(schema) {
	if (schema.enum) return "ENUM"
	switch (schema.type) {
		case "string" : return "STRING"
		case "number" : return "FLOAT"
		case "integer": return "INT"
		case "boolean": return "BOOLEAN"
		case "object" : return "OBJECT"
	}
}


function ParameterWidget({schema, state}) {
	if (schema.type === "object") return <div className="property-group">
		<div className="property-type">OBJECT</div>
		<div className="property-group-title">{schema.title}</div>
		<br/>
		{Object.keys(schema.properties).map(key => {
			const _schema = schema.properties[key]
			return <ParameterWidget key={key} schema={_schema} state={state}/>
		})}
	</div>

	return <div className="property-row">
		<div>{schema.title}</div>
		<div className="property-type">{resolveType(schema)}</div>
		<ParameterInput {...{schema, state}}/>
	</div>
}

const all = {
	type: "object",	title: "strategy config",  properties: {
		schema1: { $id: "field1",  type: "string",  title: "Title 1" },
		schema2: { $id: "field2",  type: "number",  title: "Title 2" },
		schema3: { $id: "field3",  type: "integer", title: "Title 3" },
		schema4: { $id: "field4",  type: "boolean", title: "Title 3" },
		schema5: { $id: "field5",  type: "string",  title: "Title 5",  
				  enum: [ "1", "2", "3" ], enumNames: [ "1A", "2A", "3A" ], }
	}
}

function PropertyPage() {
	const p = createLocalStoragePersistence("my:property")
	const o = useComplexState(p);




	return <div style={{ display: "flex" }}>
		<pre style={{ width: 400 }}>
			{JSON.stringify(all, null, 4)}
		</pre>
		<div>
		<div className="property-page">
		<ParameterWidget state={o} schema={all}/>
		<pre>{JSON.stringify(o.state, null, 4)}</pre>
		</div>
		</div>
	</div>

}

ReactDOM.render(<PropertyPage/>, document.querySelector("#root"));


function createLocalStoragePersistence(property: string) {

	const get = () => { 
		try {
			const t = localStorage.getItem(property)
			if (!t) return {}
			return JSON.parse(t)  
		} catch { return {} } 
	}

	const set = v => {
		try { 
			localStorage.setItem(property, JSON.stringify(v))   
		} catch {} 
	}

	return { get, set }

}