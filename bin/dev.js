const Koa = require('koa');
const path = require('path');
const http = require('http');
const koaWebpack = require('koa-webpack');
const opn = require('opn');
const webpackConfig = require('../webpack.config.js');

const app = new Koa();
app.use(require('koa-body')())
const options = {};

void async function () {
        const middleware = await koaWebpack(options)
	app.use(middleware);
	app.use(async (ctx) => {
		const filename = path.resolve(webpackConfig.output.path, 'index.html')
		ctx.response.type = 'html'
		ctx.response.body = middleware.devMiddleware.fileSystem.createReadStream(filename)
	});
	
	const server = http.createServer(app.callback())

        server.listen(() => {
            const { port } = server.address();
            console.log('http://localhost:'+port+"/");
            opn('http://localhost:'+port+"/")
        });

} ()
