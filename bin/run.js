const Koa = require('koa');
const path = require('path');
const http = require('http');
const fs = require('fs');

const app = new Koa();

app.use(require('koa-body')())
app.use(async (ctx) => {
  if (ctx.path.endsWith("js")) {
    const filename = path.join(__dirname, '../dist/bundle.js')
    ctx.response.type = 'js'
    ctx.response.body = fs.createReadStream(filename)
  } else {
    const filename = path.join(__dirname, '../dist/index.html')
    ctx.response.type = 'html'
    ctx.response.body = fs.createReadStream(filename)
  }
});
const server = http.createServer(app.callback())

server.listen(() => {
  const { port } = server.address();
  console.log('OPEN http://localhost:'+port+"/");
});

