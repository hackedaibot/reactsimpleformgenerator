const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const webpack = require("webpack");

module.exports = {
    entry: ["./src/index.tsx"],
    mode: "production",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist",
        publicPath: "/"
    },
    resolve: { extensions: [".ts", ".tsx", ".js", ".json"] },
    module: {
        rules: [
	    { test: /\.less$/, use: ['style-loader', 'css-loader', 'less-loader'] },
            { test: /\.css$/,  use: ['style-loader', 'css-loader'] },
            { test: /\.tsx?$/, loader: "awesome-typescript-loader", 
              options: { transpileOnly: true  }, exclude: /node_modules/ }
        ]
    },
    plugins: [
      new webpack.DefinePlugin({ 'process.env': { NODE_ENV: JSON.stringify('production') }}),
      new HtmlWebpackPlugin({ template: './src/index.html', inlineSource: '.(js|css)$' }),
      new HtmlWebpackInlineSourcePlugin(),
    ]
};
